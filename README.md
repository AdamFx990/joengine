# Jo-Engine

This is a modified version of <a href="https://github.com/johannes-fetz/joengine">the original Jo-Engine</a>. Only the engine and compiler are included in this repository.


This version is fully compatible with the original Jo-Engine, with only minor additions.


New code is formatted using <a href="https://llvm.org/docs/CodingStandards.html">the LLVM coding style.</a>